package test

import "fmt"

func SayHi(name string) string {
	return fmt.Sprintf("v1.0.0 Gitee Service: Hi, %s", name)
}
